SYSTEM_PYTHON = $(or $(shell which python3), $(shell which python))
PYTHON = $(or $(wildcard venv/bin/python), $(SYSTEM_PYTHON))

DOCKER_IMAGE_NAME=andreclaudino/kueski-split-and-balancer
DOCKER_IMAGE_TAG=1.0.0

venv:
	$(SYSTEM_PYTHON) -m venv $(VENV)

venv/install: venv
	$(PYTHON) -m pip install -e .
	touch venv/install

docker/package: venv/install
	$(PYTHON) setup.py bdist_wheel --dist-dir=docker/package
	rm -rf build

docker/image: docker/package
	docker build docker -f docker/Dockerfile -t $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/image

docker/login:
	docker login
	touch docker/login

docker/push: docker/image docker/login
	docker push $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG)
	touch docker/push

docker/push-latest: docker/image docker/login
	docker tag $(DOCKER_IMAGE_NAME):$(DOCKER_IMAGE_TAG) $(DOCKER_IMAGE_NAME):latest
	docker push $(DOCKER_IMAGE_NAME):latest
	touch docker/push-latest

venv/uninstall:
	$(PYTHON) -m pip uninstall feature-engineering

clean-env:
	deactivate
	rm -rf venv

clean:
	rm -rf venv/activate
	rm -rf venv/install
	rm -rf venv/uninstall

	rm -rf docker/login

	rm -rf docker/package
	rm -rf docker/image
	rm -rf docker/push
	rm -rf docker/push-latest
	rm -rf docker/install
