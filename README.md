## Kueski Balancer

Used to balance data and split into train and test parts.

```
Usage: run-balance [OPTIONS]

Options:
  -s, --source-glob TEXT          Glob pattern to source data
  --train-output-folder TEXT      Path to where train data will be saved
  --balanced-validation-output-folder TEXT
                                  Path to where balnced validation data will
                                  be saved
  --unbalanced-validation-output-folder TEXT
                                  Path to where unbalanced validation data
                                  (only raw data) will be saved
  --train-validation-proportion FLOAT
                                  The percentage of dataframe used for
                                  validation
  --spark-master TEXT             Spark master string
  --s3-endpoint TEXT              Endpoint to S3 objet storage server,
                                  defaults to S3_ENDPOINT environment variable
  --aws-access-key-id TEXT        Access key to S3 object storage server,
                                  defaults to AWS_ACCESS_KEY_ID environment
                                  variable
  --aws-secret-access-key TEXT    Access key to S3 object storage server,
                                  defaults to AWS_SECRET_ACCESS_KEY
                                  environment variable
  --max-records-per-file INTEGER  Maximum number of registers on each file.
                                  Small values are better for object
                                  storagepayload restrictions
  --help                          Show this message and exit.
```