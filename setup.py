from distutils.core import setup

from setuptools import find_packages

setup(
    name='kueski-model-training',
    version='1.0.0',
    packages=find_packages(),
    url='https://github.com/andreclaudino-kueski/feature-engineering',
    license='MIT',
    author='André Claudino',
    author_email='claudino@',
    description='A software for training the model of Kueski challenge',
    install_requires=[
        "pyspark==3.2.1",
        "click==8.1.0",
        "smart-open[all]==5.2.1",
    ],
    entry_points={
        'console_scripts': [
            "run-balance=kueski_data_balancer.main:main"
        ]
    }
)
