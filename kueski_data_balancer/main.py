import logging
import os

import click

from kueski_data_balancer.processor.asmote import make_final_dataset
from kueski_data_balancer.persistence.data import load_source_data, save_json_dataset
from kueski_data_balancer.processor.splitter import split_balanced_dataset
from kueski_data_balancer.spark_helpers import get_from_environment, create_spark_session


@click.command()
@click.option("--source-glob", "-s", type=click.STRING, help="Glob pattern to source data")
@click.option("--train-output-folder", type=click.STRING, help="Path to where train data will be saved")
@click.option("--balanced-validation-output-folder", type=click.STRING,
              help="Path to where balnced validation data will be saved")
@click.option("--unbalanced-validation-output-folder", type=click.STRING,
              help="Path to where unbalanced validation data (only raw data) will be saved")
@click.option("--train-validation-proportion", type=click.FLOAT, default=0.3,
              help="The percentage of dataframe used for validation")
@click.option("--spark-master", type=click.STRING, help="Spark master string", default="local[*]")
@click.option("--s3-endpoint", type=click.STRING,
              help="Endpoint to S3 objet storage server, defaults to S3_ENDPOINT environment variable",
              default=get_from_environment("S3_ENDPOINT"))
@click.option("--aws-access-key-id", type=click.STRING,
              help="Access key to S3 object storage server, defaults to AWS_ACCESS_KEY_ID environment variable",
              default=get_from_environment("AWS_ACCESS_KEY_ID"))
@click.option("--aws-secret-access-key", type=click.STRING,
              help="Access key to S3 object storage server, defaults to AWS_SECRET_ACCESS_KEY environment variable",
              default=get_from_environment("AWS_SECRET_ACCESS_KEY"))
@click.option("--max-records-per-file", type=click.INT, default=1000,
              help="Maximum number of registers on each file. Small values are better for object storage"
                   "payload restrictions")
def main(source_glob: str, train_output_folder: str, balanced_validation_output_folder: str,
         unbalanced_validation_output_folder: str, train_validation_proportion: float,
         spark_master: str, s3_endpoint: str, aws_access_key_id: str, aws_secret_access_key: str,
         max_records_per_file: int):

    logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    spark_session = create_spark_session(spark_master, s3_endpoint, aws_access_key_id, aws_secret_access_key)
    source_data_frame = load_source_data(spark_session, source_glob)

    train_dataset, validation_dataset = split_balanced_dataset(source_data_frame, train_validation_proportion)

    balanced_train_dataset = make_final_dataset(spark_session, train_dataset, True)
    save_json_dataset(balanced_train_dataset, train_output_folder, max_records_per_file)

    unbalanced_validation_dataset = make_final_dataset(spark_session, validation_dataset, False)
    save_json_dataset(unbalanced_validation_dataset, unbalanced_validation_output_folder, max_records_per_file)

    balanced_validation_dataset = make_final_dataset(spark_session, validation_dataset, True)
    save_json_dataset(balanced_validation_dataset, balanced_validation_output_folder, max_records_per_file)

    logging.info(f"Process finished")


if __name__ == '__main__':
    main()
