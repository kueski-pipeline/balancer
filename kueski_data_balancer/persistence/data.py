import logging

from pyspark.sql import SparkSession, DataFrame


def load_source_data(spark_session: SparkSession, source_glob: str) -> DataFrame:
    logging.info(f"Loading source data from {source_glob}")
    source_data_frame = spark_session.read.json(source_glob)

    return source_data_frame


def save_json_dataset(output_dataset: DataFrame, output_folder: str, max_records_per_file: int):
    logging.info(f"Saving dataset to {output_folder}")
    output_dataset \
        .write \
        .option("maxRecordsPerFile", max_records_per_file) \
        .mode("append") \
        .option("mapreduce.fileoutputcommitter.algorithm.version", "2") \
        .json(output_folder)
