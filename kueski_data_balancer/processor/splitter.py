import logging
import random
from typing import Tuple

from pyspark.sql import DataFrame
from pyspark.sql.functions import lit, col


def split_balanced_dataset(balanced_dataset: DataFrame, train_validation_proportion) -> Tuple[DataFrame, DataFrame]:
    logging.info(f"Splitting train and test")

    validation_dataset, train_dataset = balanced_dataset\
        .randomSplit([train_validation_proportion, 1.0-train_validation_proportion])

    return validation_dataset, train_dataset
