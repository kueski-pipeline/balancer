import logging
import random

from pyspark.ml.feature import VectorAssembler
from pyspark.ml.functions import vector_to_array
from pyspark.sql import SparkSession, DataFrame
from pyspark.sql.functions import col, lit


def make_final_dataset(spark_session: SparkSession, data_frame: DataFrame, balance) -> DataFrame:
    logging.info(f"Creating a balanced dataset")

    feature_columns = ['age', 'years_on_the_job', 'nb_previous_loans', 'avg_amount_loans_previous', 'flag_own_car']
    feature_assembler = VectorAssembler(outputCol="features", inputCols=feature_columns)

    feature_dataframe = data_frame\
        .withColumn("age", col("age").cast("double")) \
        .withColumn("years_on_the_job", col("years_on_the_job").cast("double")) \
        .withColumn("nb_previous_loans", col("nb_previous_loans").cast("double")) \
        .withColumn("avg_amount_loans_previous", col("avg_amount_loans_previous").cast("double")) \
        .withColumn("flag_own_car", col("flag_own_car").cast("double")) \
        .withColumn("status", col("status").cast("double"))

    feature_dataframe = feature_assembler\
        .transform(feature_dataframe)\
        .withColumn("label", col("status"))\
        .select("features", "label")

    if balance:
        perc_over = _calculate_perc_over(data_frame)
        asmote = _make_asmote_instance(spark_session, perc_over)
        java_balanced_dataframe = asmote.transform(feature_dataframe._jdf)
        py_balanced_dataframe = DataFrame(java_balanced_dataframe, spark_session) \
            .withColumn("features", vector_to_array(col("features"))) \
            .withColumn("label", col("label").cast("integer"))

        return py_balanced_dataframe.orderBy(lit(random.random()))
    else:
        final_data_frame = feature_dataframe \
            .withColumn("features", vector_to_array(col("features"))) \
            .withColumn("label", col("label").cast("integer"))

        return final_data_frame.orderBy(lit(random.random()))


def _make_asmote_instance(spark_sesison: SparkSession, perc_over: int):
    """
    Import approximated SMOTE from jvm package
    :param spark_sesison:
    :param perc_over:
    :return:
    """
    asmote_instance = spark_sesison.sparkContext._jvm \
        .org.apache.spark.ml.instance \
        .ASMOTE()\
        .setPercOver(perc_over)

    return asmote_instance


def _calculate_perc_over(source_dataframe: DataFrame) -> int:
    values = source_dataframe \
        .groupby("status") \
        .count()\
        .select("count")\
        .sort("count")

    smallest = values.sort("count").collect()[0].asDict()['count']
    biggest = values.sort("count").collect()[1].asDict()['count']

    delta = biggest-smallest

    perc_over = int((delta/smallest)*100)

    return perc_over
