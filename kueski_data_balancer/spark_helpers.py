import logging
import os
from typing import Optional, Callable

from pyspark.sql import SparkSession

EXTRA_JARS = ",".join([
    "com.amazonaws:aws-java-sdk-bundle:1.11.901",
    "org.apache.hadoop:hadoop-aws:3.3.1",
    "mjuez:approx-smote:1.1.0"
])


def create_spark_session(spark_master: str, s3_endpoint: Optional[str],
                         aws_access_key_id: Optional[str], aws_secret_access_key: Optional[str]) -> SparkSession:
    logging.info("Starting and configuring spark session")

    spark_session_builder = SparkSession \
        .builder \
        .master(spark_master) \
        .config("spark.jars.packages", EXTRA_JARS)

    if s3_endpoint:
        spark_session_builder.config("spark.hadoop.fs.s3a.endpoint", s3_endpoint)

    if aws_access_key_id:
        spark_session_builder.config("spark.hadoop.fs.s3a.access.key", aws_access_key_id)

    if aws_secret_access_key:
        spark_session_builder.config("spark.hadoop.fs.s3a.secret.key", aws_secret_access_key)

    spark_session_builder.config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem")
    spark_session_builder.config("spark.hadoop.fs.s3a.path.style.access", "true")
    spark_session_builder.config("spark.hadoop.fs.s3a.multipart.size", "1024")

    spark_session = spark_session_builder.getOrCreate()

    spark_session.sparkContext.setLogLevel("ERROR")

    return spark_session


def get_from_environment(environment: str) -> Callable[[], Optional[str]]:
    def get_default():
        return os.environ.get(environment)

    return get_default
